# Группа:  M80-307Б
# Студент: Мхитарян С.А.
# Условия:  Написать и отладить программу, строящую изображение заданной замечательной кривой.
# Вариант: 10
# Формула: ρ = a * exp(k * ϕ), ϕ ≤ B

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons


# Begin values
a_BEGIN = 1
k_BEGIN = 1
B_BEGIN = 2

# Initialization
a = a_BEGIN
k = k_BEGIN
B = B_BEGIN

# ϕ from 0 to B
phi = np.pi * np.arange(0, B, 0.01)

# Formula
rho = a * np.exp(k * phi)

# Plot graph, setting indents for controls
figure = plt.figure()
ax = plt.subplot(111, projection='polar')
plt.subplots_adjust(bottom=0.25)

# Plot line
line, = ax.plot(phi, rho, linewidth=3)

# Formula in title
ax.set_title(r'$\rho = a e^{k \varphi}, \varphi \leqslant\, B$', va='bottom')

# Setting axes and captions for them
ax.set_rmax(5000)
ax.set_rticks(list(range(1000, 5000, 1000)))
ax.set_rlabel_position(-22.5)
ax.grid(True)

# Position for sliders on display
ax_a = plt.axes([0.2, 0.2, 0.65, 0.03])
ax_k = plt.axes([0.2, 0.15, 0.65, 0.03])
ax_B = plt.axes([0.2, 0.1, 0.65, 0.03])

# Create sliders
slider_a = Slider(ax_a, 'a', 0.5, 10.0, valinit=a_BEGIN, valstep=0.5)
slider_k = Slider(ax_k, 'k', 0.5, 10.0, valinit=k_BEGIN, valstep=0.5)
slider_B = Slider(ax_B, 'B', 0.1, 10.0, valinit=B_BEGIN, valstep=0.1, valfmt='%.1f$\pi$')

# Update graph after change values of sliders
def update(val):
    a = slider_a.val
    k = slider_k.val
    B = slider_B.val
    r = np.arange(0, B, 0.01)
    phi = np.pi * r
    rho = a * np.exp(k * phi)
    line.set_xdata(phi)
    line.set_ydata(rho)
    figure.canvas.draw_idle()

# If value of sliders change - setup function 'update'
slider_a.on_changed(update)
slider_k.on_changed(update)
slider_B.on_changed(update)

# Create reset button
resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color='#ffffff', hovercolor='0.975')

# Reset parameters
def reset(event):
    slider_a.reset()
    slider_k.reset()
    slider_B.reset()
button.on_clicked(reset)

plt.show()