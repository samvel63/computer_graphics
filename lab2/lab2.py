# Группа:  M80-307Б
# Студент: Мхитарян С.А.
# Условия: Разработать формат представления многогранника
# Вариант: 10
# Задание: 8-ми гранная прямая правильная призма

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection


# Input values
size, transparency = (float (i) for i in input().split())
color = (0, 0, 0, transparency)

# Initialazing points of octagonal prism
points = np.array([[1, 2.5, 1],
  [2.5, 1, 1],
  [2.5, -1, 1],
  [1, -2.5, 1],
  [-1, -2.5, 1],
  [-2.5, -1, 1],
  [-2.5, 1, 1],
  [-1, 2.5, 1],
  [1, 2.5, -1],
  [2.5, 1, -1],
  [2.5, -1, -1],
  [1, -2.5, -1],
  [-1, -2.5, -1],
  [-2.5, -1, -1],
  [-2.5, 1, -1],
  [-1, 2.5, -1]])
points *= size

# Plot vertices
figure = plt.figure()
ax = figure.add_subplot(111, projection='3d')
ax.scatter3D(points[:, 0], points[:, 1], points[:, 2])

# 10 sides of octagonal prism: 8 tetragons and 2 octagons
sides = [[points[0], points[1], points[2], points[3],
    points[4],points[5],points[6],points[7]],
  [points[8], points[9], points[10], points[11],
    points[12],points[13],points[14],points[15]],
  [points[0],points[1],points[9],points[8]], 
  [points[1],points[2],points[10],points[9]],
  [points[2],points[3],points[11],points[10]], 
  [points[3],points[4],points[12],points[11]], 
  [points[4],points[5],points[13],points[12]],
  [points[5],points[6],points[14],points[13]],
  [points[6],points[7],points[15],points[14]],
  [points[7],points[0],points[8],points[15]]]

# Plot sides
ax.add_collection3d(Poly3DCollection(sides, linewidths=1,
  facecolors=color, edgecolors='k'))

ax.set_aspect('equal')
ax.set_title('Octagonal  straight prism')

ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')

plt.show()